/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.reto.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

/**
 *
 * @author _jvivasm
 */
public class Utils {
    public static <T> List<T> obtenerResultado(Class<T> clase,String path) throws FileNotFoundException{
        Gson gson = new Gson();
        List<T> result = gson.fromJson(new FileReader(path), TypeToken.getParameterized(List.class, clase).getType());
        
        return result;
    }
}
