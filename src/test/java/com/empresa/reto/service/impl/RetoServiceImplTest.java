/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.reto.service.impl;

import com.empresa.reto.dto.TemplateResponse;
import com.empresa.reto.repository.impl.RetoRepositoryImpl;
import com.empresa.reto.utils.Utils;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;
import org.springframework.boot.test.context.SpringBootTest;

/**
 *
 * @author _jvivasm
 */
@SpringBootTest
public class RetoServiceImplTest {
    
    /**
     * 
     * @throws FileNotFoundException 
     */
    @Test
    public void reestructurarTest01() throws FileNotFoundException {
        
        RetoRepositoryImpl dataRepositoryMock = mock(RetoRepositoryImpl.class);
        String path = "src/main/resources/test/data/data_repository.json";
        List<TemplateResponse> result = Utils.obtenerResultado(TemplateResponse.class,path);
        when(dataRepositoryMock.obtenerResponse()).thenReturn(result);
        
        RetoServiceImpl serviceImpl = new RetoServiceImpl(dataRepositoryMock);
        List<String> response = serviceImpl.reestructurar();
        
        assertEquals(500, response.size());

    }
    
    @Test
    public void reestructurarTest02(){
        
        RetoRepositoryImpl dataRepositoryMock = mock(RetoRepositoryImpl.class);
        when(dataRepositoryMock.obtenerResponse()).thenReturn(new ArrayList<>());
        
        RetoServiceImpl serviceImpl = new RetoServiceImpl(dataRepositoryMock);
        List<String> response = serviceImpl.reestructurar();
        assertEquals(0, response.size());

    }
    
    
    
}
