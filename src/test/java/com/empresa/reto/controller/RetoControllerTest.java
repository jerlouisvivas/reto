/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.reto.controller;

import com.empresa.reto.dto.Response;
import com.empresa.reto.service.impl.RetoServiceImpl;
import com.empresa.reto.utils.Utils;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import org.springframework.http.ResponseEntity;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

/**
 *
 * @author _jvivasm
 */
@SpringBootTest
public class RetoControllerTest {
    
    /**
     * 
     * @throws FileNotFoundException 
     */
    @Test
    public void consultarTest01() throws FileNotFoundException {
        
        RetoServiceImpl dataServiceMock = mock(RetoServiceImpl.class);
        String path = "src/main/resources/test/data/data_service.json";
        List<String> result = Utils.obtenerResultado(String.class,path);
        when(dataServiceMock.reestructurar()).thenReturn(result);
        
        RetoController controller = new RetoController(dataServiceMock);
        ResponseEntity<Response> response =  (ResponseEntity<Response>) controller.consultar();
        
        
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertThat(response.getBody()).isInstanceOf(Response.class);
        assertEquals(500, response.getBody().getData().size());

    }
    
    @Test
    public void consultarTest02() {
        
        RetoServiceImpl dataServiceMock = mock(RetoServiceImpl.class);
        when(dataServiceMock.reestructurar()).thenReturn(new ArrayList<>());
        
        RetoController controller = new RetoController(dataServiceMock);
        ResponseEntity<Response> response =  (ResponseEntity<Response>) controller.consultar();
        
        
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertThat(response.getBody()).isInstanceOf(Response.class);
        assertEquals(0, response.getBody().getData().size());

    }
}
