package com.empresa.reto.service.impl;

import com.empresa.reto.dto.TemplateResponse;
import com.empresa.reto.repository.RetoRepository;
import com.empresa.reto.service.RetoService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author _jvivasm
 */
@Service
@AllArgsConstructor
public class RetoServiceImpl implements RetoService {

    private final RetoRepository repository;

    @Override
    public List<String> reestructurar() {
        List<TemplateResponse> templResponse = this.repository.obtenerResponse();
        try {
            
            return templResponse.stream().map(tr -> {
                return tr.getPostId().concat("|").concat(tr.getId()).concat("|").concat(tr.getEmail());
            }).collect(Collectors.toList());
            
        } catch (NullPointerException e) {
            
            System.out.println(e.getMessage());
            return new ArrayList<>();
            
        }
    }
}
