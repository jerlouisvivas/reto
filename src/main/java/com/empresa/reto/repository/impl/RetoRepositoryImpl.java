/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.reto.repository.impl;

import com.empresa.reto.dto.TemplateResponse;
import com.empresa.reto.repository.RetoRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author _jvivasm
 */
@Repository
public class RetoRepositoryImpl implements RetoRepository{
    
    private final String url = "https://jsonplaceholder.typicode.com/comments";
    
    private final RestTemplate restTemplate = new RestTemplate();

    @Override
    public List<TemplateResponse> obtenerResponse() {
        
        try {
            ResponseEntity<TemplateResponse[]> respTemplate = restTemplate.getForEntity(url, TemplateResponse[].class);
            
            TemplateResponse[] body = respTemplate.getBody();
            
            if(body != null){
                return Arrays.asList(respTemplate.getBody());
            }
            
            return new ArrayList<>();
            
        } catch (RestClientException e) {
            System.out.println(e.getMessage());
            return new ArrayList<>();
        }  
       
    }
    
}
